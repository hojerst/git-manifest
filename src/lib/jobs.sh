job_output() {
  if [[ ${args['--log-mode']} == 'instant' ]]; then
    log_prefix "$(blue "[$1]") "
  else
    log="$(log_prefix "   " </dev/stdin)"
    # get lock from fd 3
    read -r -u 3 x
    echo "$(blue $"$1\n")$log"
    # release lock
    echo "$x" >&3
  fi
}

job_handle_error() {
  rc=$?

  if [[ "${args[--failfast]-x}" != x ]]; then
    return $rc
  else
    if [[ $rc -gt ${job_max_rc:-0} ]]; then
      job_max_rc="$rc"
    fi
    return 0
  fi
}

# add a single command/function to the background queue
job_add() {
  local name="$1"
  shift

  # check if fd 3 is already initialized (acts as exclusive lock)
  if [[ ${args['--log-mode']} != 'instant' ]]; then
    if ! { >&3; } 2>/dev/null; then
      # init a pipe in file descriptor 3
      local pipe
      pipe="$(mktemp -u)"
      mkfifo "$pipe"
      exec 3<>"$pipe"
      rm -f "$pipe"
      echo . >&3
    fi
  fi

  # wait for free job slot
  while [[ $(jobs | wc -l) -ge ${args['--jobs']} ]]; do
    # wait for a job to finish
    wait -n || job_handle_error
  done

  ( ("$@") |& job_output "$name") &
}

# wait for all jobs to finish (and handle errors)
job_wait_all() {
  while [[ $(jobs | wc -l) -gt 0 ]]; do
    # wait for next job to finish and handle its error code
    wait -n || job_handle_error
  done
  return ${job_max_rc:-0}
}
