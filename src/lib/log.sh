print_info() {
  yellow "$1"
}

print_error() {
  echo "$(red error): $1"
}

log_prefix() {
  sed -e "s#^#${1//#/\\#}#"
}
