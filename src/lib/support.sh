# try to find folder in this or a parent folder
find_up() {
  (
    while true; do
      if [[ -d $1 ]]; then
        echo "$PWD/$1"
        return 0
      fi
      if [[ $PWD == / ]] || [[ $PWD == // ]]; then
        return 1
      fi
      cd ..
    done
  )
}

# load manifest from $manifest_dir and check supported version
#
# post conditions:
#   repos: array of repo names
#   repo_urls: associative array of repo urls by name
load_manifest() {
  local _repos _repo _url _name
  declare -gA repo_urls

  if [ ! -d "${manifest_dir}" ]; then
    print_error "no manifest repo found - run 'git manifest clone REPO' to initialize first"
    return 1
  fi

  if [ ! -f "${manifest_dir}/manifest.yml" ]; then
    print_error "no manifest.yml present - exiting"
    return 1
  fi

  manifest="$(cat "${manifest_dir}/manifest.yml")"

  # check version
  if [ ! $(yq e '.version==1' - <<<"$manifest") = "true" ]; then
    print_error "not a supported manifest"
    return 1
  fi

  # check if there is at least one 'repos' defined
  if [ ! $(yq e '.repos | length' - <<<"$manifest") -gt 0 ]; then
    print_error "no 'repos' defined for cloning"
    return 1
  fi

  readarray _repos <<<"$(yq -jI0 e '.repos[]' - <<<"$manifest")"

  # parse repo urls
  repo_urls=()
  for _repo in "${_repos[@]}"; do
    _url="$(yq e ".url" - <<<"$_repo")"
    _name="$(basename "$_url" .git)"

    repo_urls["$_name"]="$_url"
  done
}

git() {
  if [[ -z ${NO_COLOR+x} ]]; then
    command git -c color.ui=always "$@"
  else
    command git "$@"
  fi
}
