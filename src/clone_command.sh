# redefine manifest_dir as we want to ignore existing ones in parents
manifest_dir="$PWD/.manifest"

[ -d "$manifest_dir" ] && die "manifest dir ${manifest_dir} already exists"

git clone "${args[source]}" "$manifest_dir"
load_manifest

clone_task() {
  git clone "${repo_urls[$repo]}" "$repo"
}

# iterate through repos
for repo in "${!repo_urls[@]}"; do
  if [ -d "$repo" ]; then
    print_info "already present - skipping" | job_output "$repo"
  else
    job_add "$repo" clone_task
  fi
done

# wait for background jobs and exit with highest RC
job_wait_all
