[ -d "${manifest_dir}" ] || die "no manifest found"

# update manifest folder
if [[ ${args['--skip-manifest']-x} == x ]]; then
  manifest_task() {
    cd "${manifest_dir}"
    git pull --rebase --prune --all
  }

  # use job system so we get error and output handling
  job_add ".manifest" manifest_task
  job_wait_all
fi

# go to parent for further cloning/updating
cd "$(dirname "${manifest_dir}")"

# load updated manifest
load_manifest

pull_task() {
  (cd "$repo" && git pull --rebase --prune --all)
}

clone_task() {
  git clone "${repo_urls[$repo]}" "$repo"
}

# iterate through repos
for repo in "${!repo_urls[@]}"; do
  if [ -d "$repo" ]; then
    job_add "$repo" pull_task
  else
    job_add "$repo" clone_task
  fi
done

# wait for background jobs and exit with highest RC
job_wait_all
