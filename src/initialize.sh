## Code here runs inside the initialize() function
## Use it for anything that you need to run before any other function, like
## setting environment vairables:
## CONFIG_FILE=settings.ini
##
## Feel free to empty (but not delete) this file.

manifest_dir="$(find_up .manifest || echo "$PWD/.manifest")"

# make sure fd 3 is closed as we might use it for locking
exec 3>&-
