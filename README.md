# git-manifest

This is simple git extension for management of multiple git repositories. It is heavily inspired by [repo], but grossly simplified and with yaml as manifest format.

* [Project website]
* [Source Code]
* [Releases]

## Prerequisites

* [yq] (version 4)
* [git]
* [bash]: (version 4) - note: MacOS ships with bash v3 and can be updated e.g. with homebrew

## Installation
           
### Homebrew
          
```bash
brew install hojerst/tap/git-manifest
```

### Manual

* Install the prerequisites
* Download `git-manifest` from the [Releases] page
* Install `git-manifest` to your `$PATH`

### Script

```bash
VERSION=1.3.1
curl -LOs https://gitlab.com/hojerst/git-manifest/-/releases/v${VERSION}/downloads/git-manifest
chmod 0755 git-manifest
sudo mv git-manifest /usr/local/bin
```

## Usage

`git-manifest` is implemented as a small bash script. It is controlled by a "manifest repository" and manages projects in a folder containing this manifest repository.

### Manifest repository

A manifest repository is just a git repository with a `manifest.yaml`. This file describes the structure of the multi-repo project.

A simple `manifest.yml` looks like this:

```yaml
# file version - must be 1
version: 1

repos:
- url: git@example.org:path/project1.git
- url: git@example.org:path/project2.git
```

### Workflow

```bash
# Clone a manifest repo to current folder
git manifest clone git@example.org:path/mymanifestproj.git

# Update an existing project by its manifest
# This updates the manifest repo, pull changes to all components and clones new repos
git manifest pull
```

### Resulting project structure

This above commands will result in a managed project struture like this:

```
.manifest/         - manifest repository
project1/          - a project
project2/          - another project
```

[repo]: https://gerrit.googlesource.com/git-repo
[yq]: https://mikefarah.gitbook.io/yq/
[git]: https://git-scm.com/
[bash]: https://www.gnu.org/software/bash/
[Project website]: https://hojerst.gitlab.io/git-manifest/
[Source Code]: https://gitlab.com/hojerst/git-manifest/
[Releases]: https://gitlab.com/hojerst/git-manifest/-/releases 
