setup_file() {
  export PROJECT_ROOT="$(realpath "$BATS_TEST_DIRNAME/..")"
  export GIT_CONFIG_GLOBAL="$BATS_TEST_DIRNAME/testdata/gitconfig"
}

setup() {
  load helpers
  load $PROJECT_ROOT/node_modules/bats-support/load.bash
  load $PROJECT_ROOT/node_modules/bats-assert/load.bash
  build_repo repo1
  build_repo repo2
  build_repo repo3
  build_manifest_repo <<EOF
version: '1'
repos:
- url: $REPO1_REPO
- url: $REPO2_REPO
- url: $REPO3_REPO
EOF
}

@test "clone: no repo exists" {
  cd_work

  run_info git-manifest clone "$MANIFEST_REPO"
  assert_success

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/repo2.txt
./repo3
./repo3/repo3.txt"
}

@test "clone: some repo exists" {
  cd_work

  mkdir -p repo1
  touch repo1/dummy.txt

  run_info git-manifest clone "$MANIFEST_REPO"
  assert_success

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/dummy.txt
./repo2
./repo2/repo2.txt
./repo3
./repo3/repo3.txt"
}

@test "clone: continues on failure by default" {
  cd_work

  rm -rf "$REPO2_REPO"

  run_info git-manifest clone "$MANIFEST_REPO" -j 1
  assert_failure

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo3
./repo3/repo3.txt"
}

@test "clone: stops on failure with --failfast" {
  cd_work

  rm -rf "$REPO2_REPO"

  run_info git-manifest clone "$MANIFEST_REPO" -j 1 --failfast
  assert_failure

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo3
./repo3/repo3.txt"
}

@test "clone: sanity check for jobs parameter" {
  cd_work

  run_info git-manifest clone "$MANIFEST_REPO" -j 0
  assert_failure
}
