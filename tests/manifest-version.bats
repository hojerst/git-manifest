setup_file() {
  export PROJECT_ROOT="$(realpath "$BATS_TEST_DIRNAME/..")"
  export GIT_CONFIG_GLOBAL="$BATS_TEST_DIRNAME/testdata/gitconfig"
}

setup() {
  load helpers
  load $PROJECT_ROOT/node_modules/bats-support/load.bash
  load $PROJECT_ROOT/node_modules/bats-assert/load.bash

  build_manifest_repo <<EOF
version: '2'
EOF
}

@test "invalid manifest version will fail" {
  cd "$BATS_TEST_TMPDIR"
  run_info git-manifest clone "$MANIFEST_REPO"
  assert_failure
}
