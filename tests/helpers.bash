cd_work() {
  mkdir -p "$BATS_TEST_TMPDIR/work"
  cd "$BATS_TEST_TMPDIR/work"
}

indent() {
  sed -e "s#^#${1//#/\\#}#"
}

print_runinfo() {
  cat <<EOF
command: $BATS_RUN_COMMAND
  status: $status
  output:
$(echo "$output" | indent "      ")
EOF
}

# run command with `run` and print info
run_info() {
  run "$@"
  print_runinfo
}

get_filetree() {
  find . -name .git -prune -o -print | sort
}

build_manifest_repo() {
  local manifest="$(cat)"

  export MANIFEST_REPO="$BATS_TEST_TMPDIR/src/manifest"
  echo "building manifest repo."
  echo "$manifest" | indent "   "
  (
    git init "$MANIFEST_REPO"
    cd "$MANIFEST_REPO"
    # redirect stdin to manifest.yml
    echo "$manifest" >manifest.yml
    git add .
    git commit -m 'Initial commit'
  ) &>/dev/null
}

# build a git repo for $1 (containing a single $1.txt file)
build_repo() {
  local name="$1"
  local repo="$BATS_TEST_TMPDIR/src/$name"
  echo "building test repo '$name'"
  (
    git init "$repo"
    cd "$repo"
    touch "$name.txt"
    git add .
    git commit -m 'Initial commit'
  ) &>/dev/null
  export "${name^^}_REPO"="$repo"
}

update_repo() {
  (
    cd "$1"
    touch newfile.txt
    git add newfile.txt
    git commit -m "added new file"
  )
}

add_repo_to_manifest() {
  (
    cd "$MANIFEST_REPO"
    echo "- url: $1" >>manifest.yml
    git commit -a -m 'added repo $1'
  )
}

git-manifest() {
  "$PROJECT_ROOT/git-manifest" "$@"
}
