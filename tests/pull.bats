setup_file() {
  export PROJECT_ROOT="$(realpath "$BATS_TEST_DIRNAME/..")"
  export GIT_CONFIG_GLOBAL="$BATS_TEST_DIRNAME/testdata/gitconfig"
}

setup() {
  load helpers
  load $PROJECT_ROOT/node_modules/bats-support/load.bash
  load $PROJECT_ROOT/node_modules/bats-assert/load.bash

  build_repo repo1
  build_repo repo2
  build_manifest_repo <<EOF
version: '1'
repos:
- url: $REPO1_REPO
- url: $REPO2_REPO
EOF

  cd_work
  git-manifest clone "$MANIFEST_REPO"
}

@test "pull: updates are pulled" {
  update_repo "$REPO1_REPO"

  run_info git-manifest pull
  assert_success

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/newfile.txt
./repo1/repo1.txt
./repo2
./repo2/repo2.txt"
}

@test "pull: new repos are cloned" {
  build_repo repo3
  add_repo_to_manifest "$REPO3_REPO"

  run_info git-manifest pull
  assert_success

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/repo2.txt
./repo3
./repo3/repo3.txt"
}

@test "pull: --skip-manifest works as expected" {
  update_repo repo2
  build_repo repo3
  add_repo_to_manifest "$REPO3_REPO"

  run_info git-manifest pull -m
  assert_success

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/newfile.txt
./repo2/repo2.txt"
}

@test "pull: continues on failure" {
  rm -rf "$REPO1_REPO"
  update_repo repo2
  build_repo repo3
  add_repo_to_manifest "$REPO3_REPO"

  run_info git-manifest pull -j 1
  assert_failure

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/newfile.txt
./repo2/repo2.txt
./repo3
./repo3/repo3.txt"
}

@test "pull: continues on manifest failure" {
  rm -rf "$MANIFEST_REPO"
  update_repo repo2

  run_info git-manifest pull -j 1
  assert_failure

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/newfile.txt
./repo2/repo2.txt"
}

@test "pull: fails early on --failfast" {
  rm -rf "$REPO2_REPO"
  update_repo "$REPO1_REPO"

  run_info git-manifest pull --failfast -j 1
  assert_failure

  run get_filetree
  assert_output ".
./.manifest
./.manifest/manifest.yml
./repo1
./repo1/repo1.txt
./repo2
./repo2/repo2.txt"
}

@test "pull: sanity check for jobs parameter" {
  cd_work

  run_info git-manifest pull -j 0
  assert_failure
}
